@extends('layout.master')

@section('judul')
    Tambah Pertanyaan
@endsection

@section('content')
    <div>
        <h2>Tambah Data</h2>
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Nama">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Umur</label>
                    <input type="number" class="form-control" name="umur" id="body" placeholder="Masukkan Umur">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Biodata</label>
                    <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan biodata"></textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
    </div>
@endsection

