@extends('layout.master')

@section('judul')
    Detail Cast : {{$cast->nama}}
@endsection

@section('content')

<h2>Nama : {{$cast->nama}}</h2>
<p>Umur : {{$cast->umur}}</p>
<p>Biodata : {{$cast->bio}}</p>

@endsection